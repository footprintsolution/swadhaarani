/**
 *
 * App
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 */

import React from 'react';
import { Helmet } from 'react-helmet';
import styled from 'styled-components';
import { Switch, Route } from 'react-router-dom';

import HomeScreen from 'containers/HomeScreen/Loadable';
import FeaturePage from 'containers/FeaturePage/Loadable';
import NotFoundPage from 'containers/NotFoundPage/Loadable';
import Header from 'components/Header';
import Footer from 'components/Footer';

import GlobalStyle from '../../global-styles';

const AppWrapper = styled.div`
  margin: 0 auto;
  display: flex;
  min-height: 100%;
  flex-direction: column;
`;

export default function App() {

  // useEffect(() => {
  //   const { innerWidth: width, innerHeight: height } = window;
  //   const c = document.getElementById('myCanvas');
  //   const ctx = c.getContext('2d');
  //   ctx.beginPath();
  //   ctx.moveTo(width, width/2);
  //   ctx.quadraticCurveTo(width, 800, 0, 200);
  //   ctx.stroke();
  //   console.log('the ctx is',c)
  // }, []);

  return (
    <AppWrapper>
      <Helmet
        titleTemplate="%s - React.js Boilerplate"
        defaultTitle="React.js Boilerplate"
      >
        <meta name="description" content="A React.js Boilerplate application" />
      </Helmet>
      {/* <canvas width="500" height="600" id="myCanvas" /> */}
      <Header />
      <Switch>
        <Route exact path="/" component={FeaturePage} />
        <Route path="/features" component={FeaturePage} />
        <Route path="" component={NotFoundPage} />
      </Switch>
      <Footer />
      <GlobalStyle />
    </AppWrapper>
  );
}
