import styled from 'styled-components';
import KitchenKit from '../../images/Cleaner.png';

export const SustainibilityContainer = styled.div`
    width: 100%;
    height: 500;
    text-align: center;
    margin-top: 40px;

    .sectionTitle {
        margin-bottom: 40px;
    }

    .productCard {
        width: 220px !important;
        height: 233px;
        border-radius: 8px;
    }

    .sliderImage {
        height: 233px;
        cursor: pointer;
        background-image: url('${KitchenKit}');
    }

`;

export const SubscriptionsContainer = styled.div`
    width: 100%;
    height: 350px;
    background: yellow;
    padding: 30px;
    display: flex;
    align-items: center;
    margin-top: 40px;
    margin-bottom: 40px;
`;

export const ViewAllContainer = styled.div`
    align-items: center;
    justify-content: center;
    display: flex;
    margin-top: 60px;

    .buttonContainer {
        background: #00a465;
        width: 120px;
        border-radius: 60px;
    }
`;
