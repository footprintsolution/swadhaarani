import React from 'react';
import ProductCarousel from 'components/Carousel';
import Paper from '@material-ui/core/Paper';
import { sustainProducts } from './constants';
import { SustainibilityContainer } from './style';
import { Typography } from '@material-ui/core';

const ImageThumb = ele => {
    if (ele.image) {
      return (
        <div
          className="sliderImage"
          onClick={null}
          role="presentation"
        />
      );
    }
    return null;
};

function SustainibilityKit(props) {
  return (
    <SustainibilityContainer>
      <Typography className="sectionTitle" variant="h5">
          <span dangerouslySetInnerHTML={{__html: props.title}}/>
      </Typography>
      <ProductCarousel slidesToShowSM={4}>
        {sustainProducts.map(ele => (
          <Paper className="productCard" key={ele.title}>
            {ImageThumb(ele)}
            <div className="downloadContent">
              <p className="productTitle">{ele.title}</p>
            </div>
          </Paper>
        ))}
      </ProductCarousel>
    </SustainibilityContainer>
  );
}

export default SustainibilityKit;
