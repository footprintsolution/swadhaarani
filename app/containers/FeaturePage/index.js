/*
 * FeaturePage
 *
 * List all the features
 */
import React from 'react';
import { Helmet } from 'react-helmet';
import SustainibilityKit from './SustainibilityKit'
import Subscriptions from './Subscriptions';
import { ViewAllContainer } from './style';
import { Button } from '@material-ui/core';

export default function FeaturePage() {
  return (
    <div>
      <Helmet>
        <title>Feature Page</title>
        <meta
          name="description"
          content="Feature page of React.js Boilerplate application"
        />
      </Helmet>
      <SustainibilityKit title="Sustainibility <br /> kits" />
      <Subscriptions />
      <SustainibilityKit title="All <br /> Products" />
      <ViewAllContainer>
      <Button className="buttonContainer">
        View all
      </Button>
      </ViewAllContainer>
    </div>
  );
}
