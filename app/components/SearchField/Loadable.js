/**
 *
 * Asynchronously loads the component for SearchField
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
