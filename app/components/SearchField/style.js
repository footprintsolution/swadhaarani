import styled from 'styled-components';

export const SearchContainer = styled.div`
    display: flex;
    width: 50%;
    background: white;
    padding: 5px;
    border-radius: 25px;

   .searchIcon {
    position: 'absolute';
    pointerEvents: 'none';
    display: 'flex';
    alignItems: 'center';
    justifyContent: 'center';
   }

   .input {
    width: 100%;
   }
`;
