/**
 *
 * SearchField
 *
 */

import React, { memo } from 'react';
import SearchIcon from '@material-ui/icons/Search';
import InputBase from '@material-ui/core/InputBase';
import { SearchContainer } from './style';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

function SearchField() {
  return (
  <SearchContainer>
    <div className="searchIcon">
      <SearchIcon />
    </div>
    <InputBase
      className="input"
      placeholder="Search by products, kits and more"
      inputProps={{ 'aria-label': 'search' }}
    />
  </SearchContainer>
  );
}

SearchField.propTypes = {};

export default memo(SearchField);
