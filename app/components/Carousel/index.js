/**
 *
 * Carousel
 *
 */

import React, { memo } from 'react';
import PropTypes from 'prop-types';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

function Carousel(props) {
  const { autoplay, className, slidesToShowSM, doteSM, arrowSM } = props;
  const settings = {
    dots: doteSM || false,
    arrows: arrowSM || false,
    infinite: true,
    autoplay,
    autoplaySpeed: 4000,
    speed: 700,
    slidesToShow: slidesToShowSM || 2,
    slidesToScroll: 1,
    initialSlide: 0,
    className: className || 'mp-slick-slider',
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          initialSlide: 2,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          initialSlide: 1,
        },
      },
    ],
  };

  return (
    <div>
      <Slider {...settings}>{props.children.map(ele => ele)}</Slider>
    </div>
  );
}

Carousel.propTypes = {
  autoplay: PropTypes.bool,
  className: PropTypes.string,
  slidesToShowSM: PropTypes.number,
  doteSM: PropTypes.bool,
  arrowSM: PropTypes.bool,
  children: PropTypes.node.isRequired,
};

export default memo(Carousel);
