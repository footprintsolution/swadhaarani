import React from 'react';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import { Typography } from '@material-ui/core';
import Divider from '@material-ui/core/Divider';
import { homeCareProducts, kitchenCareProducts } from './constants';
import { MenuContainer } from './style';


function AllProductMenu({ anchorEl, handleClose }) {
    return (
        <Menu
        id="customized-menu"
        keepMounted
        anchorEl={anchorEl}
        getContentAnchorEl={null}
        transformOrigin={{ vertical: "top", horizontal: "left" }}
        anchorOrigin={{ vertical: "bottom", horizontal: "left" }}
        open={Boolean(anchorEl)}
        onClose={handleClose}
        style={{ marginTop: 30 }}
      >
            <MenuContainer>
                <div className="listItem">
                    {getProductSection('HOME CARE PRODUCTS', homeCareProducts)}
                </div>
                <div className="listItem">
                    {getProductSection('KITCHEN CARE', kitchenCareProducts)}
                </div>
            </MenuContainer>
            <MenuContainer>
                <div className="listItem">
                    {getProductSection('PERSONAL CARE', homeCareProducts)}
                </div>
                <div className="listItem">
                    {getProductSection('SINGLE USE', kitchenCareProducts)}
                </div>
            </MenuContainer>
      </Menu>
    );
}

function getProductSection(title, productItem ) {
    return (
        <div>
            <span className="listItemTitle">
                {title}
            </span>
            <Divider className="divider" component="li" />
            {productItem.map((ele) => <MenuItem>
                <ListItemText primary={ele} />
            </MenuItem>)}
        </div>
    )
}

export default AllProductMenu;
