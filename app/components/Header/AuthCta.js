import React from 'react';
import SearchIcon from '@material-ui/icons/VerifiedUser';
import CartIcon from '@material-ui/icons/AddShoppingCartOutlined';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import { AuthContainer } from './style';

function AuthCta() {
    return (
        <AuthContainer>
            <Auth>
                <div>
                    <SearchIcon />
                    SIGN IN
                </div>
            </Auth>
            <Divider component="li"/>
            <Auth>
                <div>
                <CartIcon />
                CART
                </div>
            </Auth>
        </AuthContainer>
    )
}

const Auth = (props) => (
    <Button {...props}>{props.children}</Button>
);

export default AuthCta;
