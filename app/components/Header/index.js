import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import InputBase from '@material-ui/core/InputBase';
import { fade, makeStyles } from '@material-ui/core/styles';
import AllProducts from './Menu';
import SearchField from '../SearchField';
import AuthCta from './AuthCta';

const useStyles = makeStyles(theme => ({
  root: {
    color: 'black',
    height: 100,
    justifyContent: 'flex-start',
  },
  appImage: {
    width: '6%',
  },
  menuContainer: {
    marginLeft: 40,
  },
  menu: {
    display: 'flex',
    width: '65%',
    alignItems: 'center',
  },
  headerRight: {
    width: '35%',
    display: 'flex',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
}));

function Header() {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const handleClick = event => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
      <AppBar style={{ height: 550, width: '100%'}} position="static">
        <Toolbar className={classes.root}>
          <div className={classes.menu}>
          <Typography className={classes.appImage} variant="h6">
            Green
            Mini
            market
          </Typography>
          <div className={classes.menuContainer}>
          <Button
            aria-controls="customized-menu"
            aria-haspopup="true"
            variant="contained"
            color="primary"
            onClick={(e) => { handleClick(e)}}
          >
            ALL PRODUCTS
          </Button>
          <AllProducts anchorEl={anchorEl} handleClose={handleClose} />
          </div>
          <div className={classes.menuContainer}>
          <Button
            aria-controls="customized-menu"
            aria-haspopup="true"
            variant="contained"
            color="primary"
            onClick={(e) => { handleClick(e)}}
          >
            SUSTAINIBILITY KITS
          </Button>
          </div>
          <div className={classes.menuContainer}>
          <Button
            aria-controls="customized-menu"
            aria-haspopup="true"
            variant="contained"
            color="primary"
            onClick={(e) => { handleClick(e)}}
          >
            OUR STORY
          </Button>
          </div>
          <div className={classes.menuContainer}>
          <Button
            aria-controls="customized-menu"
            aria-haspopup="true"
            variant="contained"
            color="primary"
            onClick={(e) => { handleClick(e)}}
          >
            LET'S TALK
          </Button>
          </div>
          </div>
          <div className={classes.headerRight}>
            <SearchField />
            <AuthCta />
          </div>
        </Toolbar>
      </AppBar>
  );
}

export default Header;
