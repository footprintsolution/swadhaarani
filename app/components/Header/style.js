import styled from 'styled-components';

export const MenuContainer = styled.div`
    display: flex;
    align-items: center;
    justify-content: space-around;
    margin: 20px;

    .listItem {
        width: 350px;
        height: 220px;
        .listItemTitle {
            font-size: 14px;
        }
    }

    .divider {
        margin-top: 10px;
        background: yellow;
    }
`;

export const AuthContainer = styled.div`
    display: flex;
`;
