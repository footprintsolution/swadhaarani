export const homeCareProducts = [
    "Bulk Laundry Detergent",
    "Unpaste Zero Waste",
    "Zero Waste Dish",
    "Zero Waste Shampoo"
];

export const kitchenCareProducts = [
    "Plastic-Free Solid Body Elixir Bar",
    "Flower Cloth Napkin",
    "Lemon Reusable Produce Bag",
    "Zero Waste Shampoo"
];
